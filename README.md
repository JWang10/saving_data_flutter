# saving_data_app

Flutter project.

Practice how to save data using the following methods:

- Shared preferences
- SQLite database
- Text file

For `small amounts of discrete data`, `shared preferences` is a good option. If you have `a long list of data` items, though, a `database` is a better choice. In other situations, `saving data` in a file makes more sense. All of these are local storage options. If the app is uninstalled then the user will lose this data.

Here are some examples of things you might save using `shared preferences`:

- User selected color theme
- Whether night mode is enabled
- Preferred app language

Some data may not have been explicitly chosen by the user, but is still important to save. For example:

- Last scroll position
- Current tab
- Time length already played in an audio or video file
