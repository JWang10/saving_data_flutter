import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class HomePage_file extends StatefulWidget {
  HomePage_file({Key? key}) : super(key: key);

  @override
  _HomePage_fileState createState() => _HomePage_fileState();
}

class _HomePage_fileState extends State<HomePage_file> {
  String text = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Saving data"),
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    _read();
                  },
                  child: Text("Read"),
                ),
                SizedBox(width: 5),
                ElevatedButton(
                  onPressed: () {
                    _save();
                  },
                  child: Text("Save"),
                )
              ],
            ),
            Center(
              child: Text(
                text,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _read() async {
    try {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/my_file.txt');
      text = await file.readAsString();
      setState(() {
        print(text);
      });
    } catch (e) {
      setState(() {
        text = "Couldn't read file";
        print(text);
      });
    }
  }

  void _save() async {
    try {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/my_file.txt');
      // text = await file.readAsString();
      text = 'Hello World saved1';
      await file.writeAsString(text);
    } catch (e) {
      setState(() {
        text = "Couldn't write file";
        print(text);
      });
    }
  }
}
