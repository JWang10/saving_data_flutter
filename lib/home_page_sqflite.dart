import 'package:flutter/material.dart';
import 'package:saving_data_app/module/database_helpers.dart';

// ignore: camel_case_types
class HomePage_Sqflite extends StatefulWidget {
  const HomePage_Sqflite({Key? key}) : super(key: key);

  @override
  _HomePage_SqfliteState createState() => _HomePage_SqfliteState();
}

class _HomePage_SqfliteState extends State<HomePage_Sqflite> {
  String text = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Saving data"),
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              children: [
                ElevatedButton(
                  onPressed: () {
                    _read();
                  },
                  child: Text("Read"),
                ),
                SizedBox(width: 5),
                ElevatedButton(
                  onPressed: () {
                    _save();
                  },
                  child: Text("Save"),
                ),
                SizedBox(width: 5),
                ElevatedButton(
                  onPressed: () {
                    _readAll();
                  },
                  child: Text("Read All"),
                ),
              ],
            ),
            Center(
              child: Text(
                text,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _read() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    int rowId = 1;
    Word? word = await helper.queryWord(rowId);
    setState(() {
      if (word == null) {
        print('read row $rowId: empty');
        text = 'read row $rowId: empty';
      } else {
        print('read row $rowId: ${word.word} ${word.frequency}');
        text = 'read row $rowId: ${word.word} ${word.frequency}';
      }
    });
  }

  _save() async {
    Word word = Word();
    word.word = 'hello';
    word.frequency = 15;
    DatabaseHelper helper = DatabaseHelper.instance;
    int? id = await helper.insert(word);
    setState(() {
      print('inserted row: $id');
      text = 'inserted row: $id';
    });
  }

  _readAll() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    List<Map>? word = await helper.queryAllWords();
    setState(() {
      if (word == null) {
        print('read row  empty');
        text = 'read row empty';
      } else {
        print('read row $word');
        text = 'read row ${word}';
      }
    });
  }
}
