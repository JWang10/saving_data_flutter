import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage_SharedPreferences extends StatefulWidget {
  HomePage_SharedPreferences({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage_SharedPreferences> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Saving data"),
      ),
      body: Container(
        child: Row(
          children: [
            ElevatedButton(
              onPressed: () {
                _read();
              },
              child: Text("Read"),
            ),
            SizedBox(width: 5),
            ElevatedButton(
              onPressed: () {
                _save();
              },
              child: Text("Save"),
            ),
          ],
        ),
      ),
    );
  }
}

_read() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'my_int_key';
  final value = prefs.getInt(key) ?? 0;
  print('read: $value');
}

_save() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'my_int_key';
  final value = 42;
  prefs.setInt(key, value);
  print('saved $value');
}
