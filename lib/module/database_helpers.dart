import 'dart:async';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final String tableWords = 'words';
final String columnId = '_id';
final String columnWord = 'word';
final String columnFrequency = 'frequency';

// data model class
class Word {
  int? id;
  String? word;
  int? frequency;

  Word();

  // convenience constructor to create a Word object
  Word.fromMap(Map<dynamic, dynamic> map) {
    id = map[columnId];
    word = map[columnWord];
    frequency = map[columnFrequency];
  }

  // convenience mehtod to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{columnWord: word, columnFrequency: frequency};
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseHelper {
  static final _databaseName = 'MyDatabase.db';
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._praviteConstructor();
  static final DatabaseHelper instance = DatabaseHelper._praviteConstructor();

  // Only allow a single open connection to the database.
  // ignore: unused_field
  static Database? _database;
  Future<Database?> get database async {
    return _database != null ? _database : await _initDatabase();
  }

  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(
      path,
      version: _databaseVersion,
      onCreate: _onCreate,
    );
  }

  Future<void> _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableWords (
        $columnId INTEGER PRIMARY KEY,
        $columnWord TEXT NOT NULL,
        $columnFrequency INTEGER NOT NULL
      )
    ''');
  }

  // Database helper methods:

  Future<int> insert(Word word) async {
    Database? db = await database;
    int id = await db!.insert(tableWords, word.toMap());
    return id;
  }

  Future<Word?> queryWord(int id) async {
    Database? db = (await database)!;
    List<Map> maps = await db.query(
      tableWords,
      columns: [columnId, columnWord, columnFrequency],
      where: '$columnId = ?',
      whereArgs: [id],
    );
    if (maps.length > 0) {
      return Word.fromMap(maps.first);
    }
    return null;
  }

  Future<List<Map<dynamic, dynamic>>?> queryAllWords() async {
    Database db = (await database)!;
    var maps = await db.rawQuery('SELECT * FROM $tableWords');
    if (maps.length > 0) {
      return maps;
    }
    return null;
  }
}
